
#ifndef I2D1D_H
#define I2D1D_H
#include "Interaction.h"

#include <string>

/**
  * class I2D1D
  * 
  */

class I2D1D : public Interaction
{
public:

  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  I2D1D ();

  /**
   * Empty Destructor
   */
  virtual ~I2D1D ();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  


protected:

  // Static Protected attributes
  //  

  // Protected attributes
  //  

public:


  // Protected attribute accessor methods
  //  

protected:

public:


  // Protected attribute accessor methods
  //  

protected:


private:

  // Static Private attributes
  //  

  // Private attributes
  //  

public:


  // Private attribute accessor methods
  //  

private:

public:


  // Private attribute accessor methods
  //  

private:



};

#endif // I2D1D_H
