
#ifndef ADAPTIVETIMESTEPPING_H
#define ADAPTIVETIMESTEPPING_H

#include <string>

/**
  * class AdaptiveTimeStepping
  * 
  */

class AdaptiveTimeStepping
{
public:

  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  AdaptiveTimeStepping ();

  /**
   * Empty Destructor
   */
  virtual ~AdaptiveTimeStepping ();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  


protected:

  // Static Protected attributes
  //  

  // Protected attributes
  //  

public:


  // Protected attribute accessor methods
  //  

protected:

public:


  // Protected attribute accessor methods
  //  

protected:


private:

  // Static Private attributes
  //  

  // Private attributes
  //  

public:


  // Private attribute accessor methods
  //  

private:

public:


  // Private attribute accessor methods
  //  

private:



};

#endif // ADAPTIVETIMESTEPPING_H
