
#ifndef PETCS_H
#define PETCS_H

#include <string>

/**
  * class Petcs
  * 
  */

class Petcs
{
public:

  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  Petcs ();

  /**
   * Empty Destructor
   */
  virtual ~Petcs ();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  


protected:

  // Static Protected attributes
  //  

  // Protected attributes
  //  

public:


  // Protected attribute accessor methods
  //  

protected:

public:


  // Protected attribute accessor methods
  //  

protected:


private:

  // Static Private attributes
  //  

  // Private attributes
  //  

public:


  // Private attribute accessor methods
  //  

private:

public:


  // Private attribute accessor methods
  //  

private:



};

#endif // PETCS_H
