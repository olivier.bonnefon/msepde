
#ifndef DYNAMICSYSTEM_H
#define DYNAMICSYSTEM_H

#include <string>


class SparseMat;
/**
  * class DynamicSystem
  * A dynamical system is a part of the global system
  */

class DynamicSystem
{
public:

  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  DynamicSystem ();

  /**
   * Empty Destructor
   */
  virtual ~DynamicSystem ();

  
  // Static Public attributes
  //  

  // Public attributes
  //  
  int mNSpec;
  double * mIC;
  // state at the previous time step
  double * mStatetm1;
  // State where the system is linearized
  double * mStateLinearized;

  SparseMat * mMat;

  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  


  




  /**
   * @param  curT
   */
  virtual void preparTimeStep (double& curT,double& dt);

  /**
   * copy the IC to the global dof
   */
  void virtual icToDof ()
  {
  }


  /**
   * build the rhs of the linear system
   */
  void virtual buildRhs ()
  {
  }


  /**
   * build the matrix from the linearized system
   */
  void virtual buildMat ()
  {
  }


  int mOffsetInDofs;
  int mSizeInDofs;


};

#endif // DYNAMICSYSTEM_H
