
#ifndef DOF_H
#define DOF_H

#include <string>

/**
  * class Dof
  * Dof contains the state of the global system
  */

class Dof
{
public:

  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  Dof ();

  /**
   * Empty Destructor
   */
  virtual ~Dof ();

  // Static Public attributes
  //  

  // Public attributes
  //  

  double * mV;

  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  


  /**
   * Set the value of mV
   * @param new_var the new value of mV
   */
  void setMV (double * new_var)   {
      mV = new_var;
  }

  /**
   * Get the value of mV
   * @return the value of mV
   */
  double * getMV ()   {
    return mV;
  }


  /**
   * save dofs
   * @param  fileTarget
   */
  void saveState (string fileTarget)
  {
  }

protected:

  // Static Protected attributes
  //  

  // Protected attributes
  //  

public:


  // Protected attribute accessor methods
  //  

protected:

public:


  // Protected attribute accessor methods
  //  

protected:


private:

  // Static Private attributes
  //  

  // Private attributes
  //  

public:


  // Private attribute accessor methods
  //  

private:

public:


  // Private attribute accessor methods
  //  

private:


  void initAttributes () ;

};

#endif // DOF_H
