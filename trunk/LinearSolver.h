
#ifndef LINEARSOLVER_H
#define LINEARSOLVER_H

class LinearSystem;
/**
  * class LinearSolver
  * 
  */

class LinearSolver
{
public:

  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  LinearSolver ();

  /**
   * Empty Destructor
   */
  virtual ~LinearSolver ();

  // Static Public attributes
  //  

  // Public attributes
  //  

  LinearSystem* mLinearSystem;
  // mStatus must be set to kwon if the linear system has been correctly solved.
  // mStatus must be set to 0 if succed
  int mStatus;

  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   */
  void solve ();


};

#endif // LINEARSOLVER_H
