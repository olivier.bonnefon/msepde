
#ifndef I1D1D_H
#define I1D1D_H
#include "Interaction.h"

#include <string>

/**
  * class I1D1D
  * 
  */

class I1D1D : public Interaction
{
public:

  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  I1D1D ();

  /**
   * Empty Destructor
   */
  virtual ~I1D1D ();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  


protected:

  // Static Protected attributes
  //  

  // Protected attributes
  //  

public:


  // Protected attribute accessor methods
  //  

protected:

public:


  // Protected attribute accessor methods
  //  

protected:


private:

  // Static Private attributes
  //  

  // Private attributes
  //  

public:


  // Private attribute accessor methods
  //  

private:

public:


  // Private attribute accessor methods
  //  

private:



};

#endif // I1D1D_H
