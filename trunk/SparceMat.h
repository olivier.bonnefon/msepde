
#ifndef SPARCEMAT_H
#define SPARCEMAT_H

#include <string>

/**
  * class SparceMat
  * 
  */

class SparceMat
{
public:

  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  SparceMat ();
  SparceMat (int dim);

  /**
   * Empty Destructor
   */
  virtual ~SparceMat ();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   * add the pSM to this
   * @param  pSM matrix inserted
   */
  virtual void addSubMatrix (SparceMat * pSM)
  {
  }




};

#endif // SPARCEMAT_H
