#include "TimeSteppingDoOneStep.h"
#include "commun.h"
#include "System.h"
#include "LinearSystem.h"
#include "TimeStepping.h"
// Constructors/Destructors
//  

TimeSteppingDoOneStep::TimeSteppingDoOneStep (TimeStepping * pTS) {
  mTimeStepping=pTS;
  mDt=0.1;
}

TimeSteppingDoOneStep::~TimeSteppingDoOneStep () { }

//  
// Methods
//  
/**
   * perform one step mCurT to mCurT+mDt
   * Input :
   * - System.mDof contains the state at mCurT
   * output:
   * -System.mDof the state at mCurT+mDt
   */
double TimeSteppingDoOneStep::doOneStep (){
  BBLOC;
  mTimeStepping->mSystem->preparTimeStep(mTimeStepping->mCurT,mDt);
  mTimeStepping->mSystem->buildRhs();
  mTimeStepping->mSystem->buildMat();
  mTimeStepping->mSystem->mLinearSystem->solve();
  mTimeStepping->mSystem->solToDof();
  return mDt;
  EBLOC;
}



// Accessor methods
//  


