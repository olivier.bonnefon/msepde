
#ifndef INTERACTION_H
#define INTERACTION_H

#include <string>

/**
  * class Interaction
  * 
  */

class Interaction
{
public:

  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  Interaction ();

  /**
   * Empty Destructor
   */
  virtual ~Interaction ();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  


protected:

  // Static Protected attributes
  //  

  // Protected attributes
  //  

public:


  // Protected attribute accessor methods
  //  

protected:

public:


  // Protected attribute accessor methods
  //  

protected:


private:

  // Static Private attributes
  //  

  // Private attributes
  //  

public:


  // Private attribute accessor methods
  //  

private:

public:


  // Private attribute accessor methods
  //  

private:



};

#endif // INTERACTION_H
