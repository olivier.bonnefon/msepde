#include "TimeStepping.h"
#include "Simulator.h"
#include "System.h"
#include "TimeSteppingDoOneStep.h"
#include "commun.h"
// Constructors/Destructors
//  

TimeStepping::TimeStepping (Simulator *pS) {
  mSimulator=pS;
  mCurT = 0;
  mSystem=pS->mSystem;
  mTSDoOneStep= new TimeSteppingDoOneStep(this);
}

TimeStepping::~TimeStepping () {
  delete mTSDoOneStep;
}

//  
// Methods
//  

void TimeStepping::performSimu(){
  BBLOC;
  while (mCurT < mSimulator->mTf+MSEFEM_SMALL_DOUBLE){
    mCurT += mTSDoOneStep->doOneStep();
  }
  EBLOC;
}

void TimeStepping::initForSimulation(){
  mCurT=mSimulator->mT0;
}
// Accessor methods
//  

