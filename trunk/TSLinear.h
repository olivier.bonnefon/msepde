
#ifndef TSLINEAR_H
#define TSLINEAR_H
#include "TimeStep.h"

#include <string>

/**
  * class TSLinear
  * 
  */

class TSLinear : public TimeStep
{
public:

  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  TSLinear ();

  /**
   * Empty Destructor
   */
  virtual ~TSLinear ();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   * implementation of the linear case
   */
  virtual void solve ()
  {
  }

protected:

  // Static Protected attributes
  //  

  // Protected attributes
  //  

public:


  // Protected attribute accessor methods
  //  

protected:

public:


  // Protected attribute accessor methods
  //  

protected:


private:

  // Static Private attributes
  //  

  // Private attributes
  //  

public:


  // Private attribute accessor methods
  //  

private:

public:


  // Private attribute accessor methods
  //  

private:



};

#endif // TSLINEAR_H
