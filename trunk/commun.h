#ifndef COMMUN_H
#define COMMUN_H
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

#define MSEFEM_SMALL_DOUBLE 1e-12
#define VERBOSE_MODE_FULL

#ifdef VERBOSE_MODE_FULL
#define BBLOC  printf("BBLOC %s %i\n",__FILE__,__LINE__);  
#define EBLOC   printf("EBLOC %s %i\n",__FILE__,__LINE__);
#else
#define BBLOC
#define EBLOC
#endif

#endif
