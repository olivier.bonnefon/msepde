#ifndef DOLFINMODEL_H
#define DOLFINMODEL_H

#include "commun.h"

#include "DynamicSystem.h"
#include <dolfin.h>
using namespace dolfin;

class SparceMat;


/**
  * class DolfinModel
  * contain the variatinal formulation of the linearized system.
  * 
  * d_t u = /Delta Du + F(u)
  * 
  * Linearised at u_k:
  * 
  * d_t u = /Delta Du + F(u_k) + F'(u_k)(u-u_k)
  * 
  * d_t u = /Delta Du + F'(u_k)u+F(u_k)-F'(u_k)u_k
  * 
  * 
  * 
  * d_t u= \Delta Du + Lu +c
  * 
  * tm1 -> t
  * implicite :
  * u_t - u_{tm1} = \alpha*(D \Delta u_t + Lu_t)
  * 
  * theta-method:
  * 
  * u_t - u_{tm1} = \alpha*D \Delta u_t + (\theta)*Lu_t +
  *                               (1-\theta)*Lu_{tm1}
  * On cherche u telque pour tout v:
  * a(u,v)=L(v)
  * 
  * pour la methode implicite
  * a(u,v) = u.v + \alpha* D*grad u* grad v - \alpha*Lu.v
  * L(v) = u_tm1*v
  * 
  * 
  * 
  * 
  */

class DolfinModel : public DynamicSystem
{
public:

   // Constructors/Destructors
  //  


  /**
   *  Constructor
   */
  DolfinModel ();
  DolfinModel (string file,int nSpec);

  /**
   * Empty Destructor
   */
  virtual ~DolfinModel ();

  // Static Public attributes
  //  

  // Public attributes
  //  

  // matrix build from the variational formulation
  // 
  SparceMat *mMat;
  // Rhs of the variational formulation
  double * mRhs;
  bool mIsLinear;
  double mDt;
  double mAlpha;
  std::shared_ptr<Constant> mAlphaDolfin;
  std::shared_ptr<Function> mUtm1Dolfin;
  std::shared_ptr<Mesh> mMesh;
  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  

  virtual void preparTimeStep (double& curT,double& dt);

  /**
   * ex: save state to vtk
   */
  virtual void postStep ();



};

#endif // DOLFINMODEL_H
