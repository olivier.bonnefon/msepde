
#ifndef SYSTEM_H
#define SYSTEM_H

#include <string>

class LinearSystem;
class DynamicSystem;
/**
  * class System
  * 
  */

class System
{
public:

  // Constructors/Destructors
  //  


  /**
   *  Constructor
   */
  System (int nSpec);

  /**
   * Empty Destructor
   */
  virtual ~System ();

  virtual void setDS(DynamicSystem *pDS,int nDS);

  int mNSpec;

  DynamicSystem* mDS;
  //number of DS
  int mNDS;
  // total number of dofs
  int mNGlobalDofs;
  // contains the 'current' state of the system. During simulation it is the state at TimeStepping.mCurT
  double * mDof;
  // dof where linearized the system 
  double * mDofLinearized;
  // State at the previous time
  double * mDoftm1;
  // the linear system AX=B  
  LinearSystem * mLinearSystem;

  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   * dispatch to DynamicalSystems the update for the next time step.
   * It consists in updating the matrix of the linear system from the model
   * @param  curT
   */
  void preparTimeStep (double curT,double dt);

  /**
   * build the rhs of the linear system
   */
  void buildRhs ();


  /**
   * Build the matrix sparce of the system
   */
  void buildMat ();
 

  

  void solToDof();
  
  /**
   * something to do after each step ? do here
   * @param  curT
   */
  void postStep (double curT);



};

#endif // SYSTEM_H
