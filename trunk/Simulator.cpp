#include "Simulator.h"
#include "commun.h"
#include "TimeStepping.h"
// Constructors/Destructors
//  

Simulator::Simulator (System * pS) {
  BBLOC;
  mT0 = 0;
  mTf = 1;
  mSystem=pS;
  mTimeStepping=new TimeStepping(this);
  
  EBLOC;
}

Simulator::~Simulator () {
  delete mTimeStepping;
}

//  
// Methods
//  



void Simulator::performSimu(){
  BBLOC;
  mTimeStepping->initForSimulation();
  mTimeStepping->performSimu();
  EBLOC;
}
