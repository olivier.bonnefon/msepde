
#ifndef LINEARSTEP_H
#define LINEARSTEP_H

#include <string>

/**
  * class LinearStep
  * 
  */

class LinearStep
{
public:

  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  LinearStep ();

  /**
   * Empty Destructor
   */
  virtual ~LinearStep ();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   * implement one step for linear system.
   * 
   */
  virtual void doOneStep ()
  {
  }

protected:

  // Static Protected attributes
  //  

  // Protected attributes
  //  

public:


  // Protected attribute accessor methods
  //  

protected:

public:


  // Protected attribute accessor methods
  //  

protected:


private:

  // Static Private attributes
  //  

  // Private attributes
  //  

public:


  // Private attribute accessor methods
  //  

private:

public:


  // Private attribute accessor methods
  //  

private:



};

#endif // LINEARSTEP_H
