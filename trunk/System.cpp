#include "System.h"
#include "commun.h"
#include "DynamicSystem.h"
#include "LinearSystem.h"

// Constructors/Destructors
//  


System::System (int nSpec) {
  mNSpec=nSpec;
  mDof=0;
  mDofLinearized=0;
  mDoftm1=0;
  mLinearSystem=0;
}

System::~System () {
  free(mDof);
  free(mDofLinearized);
  free(mDoftm1);
  if (mLinearSystem)
    delete mLinearSystem;
}

//  
// Methods
//

void  System::preparTimeStep (double curT,double dt)
  {
    BBLOC;
    DynamicSystem * pDS=mDS;
    for (int n=0; n<mNDS; n++){
      pDS->preparTimeStep(curT,dt);
      pDS++;
    }
    EBLOC;
  }


void System::solToDof(){
  BBLOC;
  DynamicSystem * pDS=mDS;
  double * pSol=mLinearSystem->mSol;
  for (int n=0; n<mNDS; n++){
    memcpy(pDS->mStatetm1,pSol+pDS->mOffsetInDofs,pDS->mSizeInDofs);
    pDS++;
  }
  EBLOC;
}
void System::buildRhs (){
  BBLOC;
  DynamicSystem * pDS=mDS;
  for (int n=0; n<mNDS; n++){
    pDS->buildRhs();
    pDS++;
  }

  EBLOC;
}


  /**
   * Build the matrix sparce of the system
   */
void System::buildMat (){
  BBLOC;
  DynamicSystem * pDS=mDS;
  for (int n=0; n<mNDS; n++){
    pDS->buildMat();
    pDS++;
  }
  EBLOC;
}

void System::postStep (double curT)
  {
  }
void System::setDS(DynamicSystem *pDS,int nDS){
  BBLOC;
  mDS=pDS;
  mNDS=nDS;
  mNGlobalDofs=0;
  for (int n=0; n<mNDS; n++){
    pDS->mOffsetInDofs=mNGlobalDofs;
    mNGlobalDofs+=pDS->mSizeInDofs;
    pDS++;
  }
  mDof=(double*) malloc(mNGlobalDofs*sizeof(double));
  mDofLinearized=(double*) malloc(mNGlobalDofs*sizeof(double));
  mDoftm1=(double*) malloc(mNGlobalDofs*sizeof(double));
  mLinearSystem = new LinearSystem(mNGlobalDofs);
  EBLOC;
}
