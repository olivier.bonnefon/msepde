
#ifndef TIMESTEPDOONESTEP_H
#define TIMESTEPDOONESTEP_H

#include <string>
class TimeStepping;


/**
  * class TimeSteppingDoOneStep
  * 
  */

class TimeSteppingDoOneStep
{
public:

  // Constructors/Destructors
  //  


  /**
   *  Constructor
   */
  TimeSteppingDoOneStep (TimeStepping * );

  /**
   * Empty Destructor
   */
  virtual ~TimeSteppingDoOneStep ();

  // Static Public attributes
  //  

  // Public attributes
  //

  TimeStepping* mTimeStepping;

  
  double mDt;

  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  


  


  /**
   * perform one step mCurT to mCurT+mDt
   * Input :
   * - System.mDof contains the state at mCurT
   * output:
   * -System.mDof the state at mCurT+mDt
   */
  virtual double doOneStep ();


 

  

};

#endif 
