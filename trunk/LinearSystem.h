
#ifndef LINEARSYSTEM_H
#define LINEARSYSTEM_H

#include <string>

class LinearSolver;
class SparceMat;

/**
  * class LinearSystem
  * 
  */

class LinearSystem
{
public:

  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  LinearSystem (int dim);

  /**
   * Empty Destructor
   */
  virtual ~LinearSystem ();


  void solve();
  
  // Public attributes
  //  

  double * mSol;
  double * mRhs;
  int mDim;
  SparceMat *mMat;
  LinearSolver *mSolver;
// force the solution to be >0.
  bool mForcePositiv;
  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



};

#endif // LINEARSYSTEM_H
