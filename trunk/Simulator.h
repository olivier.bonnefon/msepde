
#ifndef SIMULATOR_H
#define SIMULATOR_H

#include <string>

class TimeStepping;
class System;

/**
  * class Simulator
  * 
  */

class Simulator
{
public:

  // Constructors/Destructors
  //  


  /**
   *  Constructor
   */
  Simulator ();
  Simulator (System * pS);

  /**
   * Empty Destructor
   */
  virtual ~Simulator ();

  // Static Public attributes
  //  

  // Public attributes
  //  

  /*Ony one here
    To Do: set to a list of TimeStepping to manage different step of simulation, deppending of the dynamical system. 
   */
  TimeStepping * mTimeStepping;
  System * mSystem;
  double mT0;
  double mTf;

  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  

 
  void performSimu();



private:

};

#endif // SIMULATOR_H
