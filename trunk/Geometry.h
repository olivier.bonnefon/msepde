
#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <string>
#include vector



/**
  * class Geometry
  * 
  */

class Geometry
{
public:

  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  Geometry ();

  /**
   * Empty Destructor
   */
  virtual ~Geometry ();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  


protected:

  // Static Protected attributes
  //  

  // Protected attributes
  //  

public:


  // Protected attribute accessor methods
  //  

protected:

public:


  // Protected attribute accessor methods
  //  

protected:


private:

  // Static Private attributes
  //  

  // Private attributes
  //  

public:


  // Private attribute accessor methods
  //  

private:

public:


  // Private attribute accessor methods
  //  

private:



};

#endif // GEOMETRY_H
