
#ifndef TIMESCHEME_H
#define TIMESCHEME_H

#include <string>
#include vector



/**
  * class TimeScheme
  * contain the caractéristics of the time discretisation algorithm.
  */

class TimeScheme
{
public:

  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  TimeScheme ();

  /**
   * Empty Destructor
   */
  virtual ~TimeScheme ();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  


protected:

  // Static Protected attributes
  //  

  // Protected attributes
  //  

public:


  // Protected attribute accessor methods
  //  

protected:

public:


  // Protected attribute accessor methods
  //  

protected:


private:

  // Static Private attributes
  //  

  // Private attributes
  //  

  // theta of the theta-method
  double mTheta;
public:


  // Private attribute accessor methods
  //  

private:

public:


  // Private attribute accessor methods
  //  


  /**
   * Set the value of mTheta
   * theta of the theta-method
   * @param new_var the new value of mTheta
   */
  void setMTheta (double new_var)   {
      mTheta = new_var;
  }

  /**
   * Get the value of mTheta
   * theta of the theta-method
   * @return the value of mTheta
   */
  double getMTheta ()   {
    return mTheta;
  }
private:


  void initAttributes () ;

};

#endif // TIMESCHEME_H
