
#ifndef TIMESTEPPING_H
#define TIMESTEPPING_H

#include <string>

class TimeSteppingDoOneStep;
class Simulator;
class System;

/**
  * class TimeStepping
  * Perform step by step the simulation
  */

class TimeStepping
{
public:

  // Constructors/Destructors
  //  


  /**
   *  Constructor
   */
  //TimeStepping ();
  TimeStepping (Simulator *);

  /**
   * Empty Destructor
   */
  virtual ~TimeStepping ();

  void initForSimulation();
  virtual void performSimu();

  

  // Static Public attributes
  //  

  // Public attributes
  //  

  System * mSystem;
  //the algorithm to do one step
  TimeSteppingDoOneStep  *mTSDoOneStep;
  Simulator* mSimulator;
  
  // current time of state of the system
  double mCurT;

  


};

#endif // TIMESTEPPING_H
