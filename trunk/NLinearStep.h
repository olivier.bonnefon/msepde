
#ifndef NLINEARSTEP_H
#define NLINEARSTEP_H

#include <string>

/**
  * class NLinearStep
  * 
  */

class NLinearStep
{
public:

  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  NLinearStep ();

  /**
   * Empty Destructor
   */
  virtual ~NLinearStep ();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  


protected:

  // Static Protected attributes
  //  

  // Protected attributes
  //  

public:


  // Protected attribute accessor methods
  //  

protected:

public:


  // Protected attribute accessor methods
  //  

protected:


private:

  // Static Private attributes
  //  

  // Private attributes
  //  

public:


  // Private attribute accessor methods
  //  

private:

public:


  // Private attribute accessor methods
  //  

private:



};

#endif // NLINEARSTEP_H
