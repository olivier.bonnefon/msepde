
#ifndef MODEL_H
#define MODEL_H
#include "DynamicSystem.h"

#include <string>

/**
  * class Model
  * 
  */

class Model : public DynamicSystem
{
public:

  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  Model ();

  /**
   * Empty Destructor
   */
  virtual ~Model ();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   * exemple : export dofs for visu
   */
  virtual void postStep ()
  {
  }

protected:

  // Static Protected attributes
  //  

  // Protected attributes
  //  

public:


  // Protected attribute accessor methods
  //  

protected:

public:


  // Protected attribute accessor methods
  //  

protected:


private:

  // Static Private attributes
  //  

  // Private attributes
  //  

public:


  // Private attribute accessor methods
  //  

private:

public:


  // Private attribute accessor methods
  //  

private:



};

#endif // MODEL_H
