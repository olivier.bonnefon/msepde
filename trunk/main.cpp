#include "MSEFEM.h"

int main(){
  int nSpec=1;
  int nDS=1;
  System * pSys = new System(nSpec);
  DolfinModel * pDS= new DolfinModel("toto.xdmf",nSpec);
  pSys->setDS(pDS,nDS);
  
  Simulator * pSim=new Simulator(pSys);
  
  

  pSim->performSimu();
  

  delete pDS;
  delete pSys;
  delete pSim;
}
