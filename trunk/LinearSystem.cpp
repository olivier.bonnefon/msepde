#include "LinearSystem.h"
#include "commun.h"
// Constructors/Destructors
//  

LinearSystem::LinearSystem (int dim) {
  BBLOC;
  mDim=dim;
  mForcePositiv=true;
  mSol=(double*)malloc(mDim*sizeof(double));
  mRhs=(double*)malloc(mDim*sizeof(double));
  EBLOC;
}

LinearSystem::~LinearSystem () {
  BBLOC;
  free(mSol);
  free(mRhs);
  EBLOC;
}

//  
// Methods
//  
void LinearSystem::solve(){
  BBLOC;
  //mSolver->solve();
  if (mForcePositiv){
    double *pAux=mSol;
    for (int i=0;i<mDim;i++){
      if (*pAux<0) *pAux=0;
      pAux++;
    }
  }
  EBLOC;
}

// Accessor methods
//  



