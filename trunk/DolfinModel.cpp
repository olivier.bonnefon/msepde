#include "DolfinModel.h"
#include "SparceMat.h"

#include "DOLFINMODEL/Simple.h"

// Constructors/Destructors
//  

DolfinModel::DolfinModel () {

}
DolfinModel::DolfinModel (string file,int nSpec) : DynamicSystem()
{
  BBLOC;
  mNSpec=nSpec;
  printf("here load mesh %s\n",file.c_str());
  mMesh=std::make_shared<Mesh>(UnitSquareMesh::create({{32, 32}}, CellType::Type::triangle));
  mSizeInDofs=mNSpec;//*nNode
  mMat= new SparceMat(mSizeInDofs);
  mIC= (double*)malloc(mSizeInDofs*sizeof(double));
  mStatetm1= (double*)malloc(mSizeInDofs*sizeof(double));
  mStateLinearized= (double*)malloc(mSizeInDofs*sizeof(double));
  mAlphaDolfin = std::make_shared<Constant>(mAlpha);
  //mUtm1Dolfin = std::make_shared<Function>;
  EBLOC;
}

DolfinModel::~DolfinModel () {
  free(mIC);
  free(mStatetm1);
  free(mStateLinearized);
  delete mMat;
}

//  
// Methods
//  
 void  DolfinModel::preparTimeStep (double& curT,double& dt){
   BBLOC;
   mDt=dt;
   mAlpha=1/dt;

   
   *mAlphaDolfin = mAlpha;
   EBLOC;
  }

  /**
   * ex: save state to vtk
   */
 void  DolfinModel::postStep ()
  {
    BBLOC;
    EBLOC;
  }

